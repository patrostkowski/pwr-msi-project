from test.adasyn import adasyn
import warnings
import numpy as np
import matplotlib.pyplot as plt
from math import pi
from tabulate import tabulate
from sklearn import datasets
from sklearn.preprocessing import MinMaxScaler
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from collections import Counter
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.base import clone
from sklearn.model_selection import train_test_split
from strlearn.metrics import recall
from strlearn.metrics import precision
from strlearn.metrics import specificity
from strlearn.metrics import f1_score
from strlearn.metrics import geometric_mean_score_1
from strlearn.metrics import balanced_accuracy_score
import warnings

warnings.filterwarnings("ignore")

datafile = 'haberman'

raw_data = np.genfromtxt(f"datasets_csv/{datafile}.csv", delimiter=";")
print(raw_data)

dataset = MinMaxScaler().fit_transform(raw_data)
print(dataset)

X = dataset[:, :-1]
y = dataset[:, -1].astype(int)

#plt.figure(figsize=(7,7))
#plt.scatter(X[:,0], X[:,1], c=y, cmap='bwr')
#plt.show()

print('before classes')

classes = np.unique(y)
n_rows, n_cols = X.shape
n_classes = len(classes)

print('N Examples: %d' % n_rows)
print('N Inputs: %d' % n_cols)
print('N Classes: %d' % n_classes)
print('Classes: %s' % classes)
print('Class Breakdown:')
for c in classes:
    total = len(y[y == c])
    ratio = (total / float(len(y))) * 100
    print(' - Class %s: %d (%.3f%%)' % (str(c), total, ratio))

print("--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ")

clf = DecisionTreeClassifier(random_state=1337)

#clf = GaussianNB()

preprocs = {
    'adasyn': adasyn(random_state=1337),
    'smote' : SMOTE(random_state=1337),    
    'ros': RandomOverSampler(random_state=1337),
    'rus': RandomUnderSampler(random_state=1337),
}
metrics = {
    "recall": recall,
    'precision': precision,
    'specificity': specificity,
    'f1': f1_score,
    'g-mean': geometric_mean_score_1,
    'bac': balanced_accuracy_score,
}

n_splits = 5
n_repeats = 2
rskf = RepeatedStratifiedKFold(
    n_splits=n_splits, n_repeats=n_repeats, random_state=1337)

results = np.zeros((len(preprocs), n_splits * n_repeats, len(metrics)))
scores = np.zeros((len(preprocs), n_splits * n_repeats, len(metrics)))

temp_X, temp_y = X, y

for preproc_id, preproc in enumerate(preprocs):
    temp_X, temp_y = preprocs[preproc].fit_resample(
        X, y)
    for fold_id, (train, test) in enumerate(rskf.split(temp_X, temp_y)):
        clf = clone(clf)
        clf.fit(temp_X[train], temp_y[train])
        y_pred = clf.predict(temp_X[test])
        for metric_id, metric in enumerate(metrics):
            scores[preproc_id, fold_id, metric_id] = metrics[metric](temp_y[test], y_pred)
            results[preproc_id, fold_id, metric_id] = accuracy_score(temp_y[test], y_pred)

# Zapisanie wynikow
np.save('scores', scores)
np.save('results', results)

print(scores.shape)
print(results.shape)

scores = np.load("scores.npy")
scores = np.mean(scores, axis=1).T

np.savetxt("scores.csv", scores, delimiter=";")

print(scores)

metrics=["Recall", 'Precision', 'Specificity', 'F1', 'G-mean', 'BAC']
methods=['ADASYN', 'SMOTE', 'ROS', 'RUS']
N = scores.shape[0]

angles = [n / float(N) * 2 * pi for n in range(N)]
angles += angles[:1]

ax = plt.subplot(111, polar=True)

ax.set_theta_offset(pi / 2)
ax.set_theta_direction(-1)

plt.xticks(angles[:-1], metrics)

ax.set_rlabel_position(0)
plt.yticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1],
["0.0","0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0"],
color="grey", size=7)
plt.ylim(0,1)

for method_id, method in enumerate(methods):
    values=scores[:, method_id].tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=method)

plt.legend(bbox_to_anchor=(1.15, -0.05), ncol=5)

plt.savefig("radar", dpi=200)

